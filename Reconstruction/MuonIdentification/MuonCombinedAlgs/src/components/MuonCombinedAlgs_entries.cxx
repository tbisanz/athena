#include "../MuonCombinedAlg.h"
#include "../MuonCombinedInDetCandidateAlg.h"
#include "../MuonCombinedInDetExtensionAlg.h"
#include "../MuonCombinedMuonCandidateAlg.h"
#include "../MuonCreatorAlg.h"
#include "../MuonSegmentTagAlg.h"

DECLARE_COMPONENT(MuonCombinedInDetCandidateAlg)
DECLARE_COMPONENT(MuonCombinedMuonCandidateAlg)
DECLARE_COMPONENT(MuonCombinedAlg)
DECLARE_COMPONENT(MuonCreatorAlg)
DECLARE_COMPONENT(MuonCombinedInDetExtensionAlg)
DECLARE_COMPONENT(MuonSegmentTagAlg)
